package abnd.pue.register.service.impl;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import abnd.pue.app.config.Constants;
import abnd.pue.auth.model.PUERole;
import abnd.pue.auth.model.PUEUser;
import abnd.pue.auth.service.impl.PUERoleManager;
import abnd.pue.auth.service.impl.PUEUserDetailsService;
import abnd.pue.auth.service.impl.PUEUserManager;
import abnd.pue.register.service.intf.RegisterService;

@Service
public class RegisterServiceImpl implements RegisterService {
	@Autowired
	private PUEUserManager userManager;
	@Autowired
	private PUERoleManager roleManager;
	
	Logger logger = LoggerFactory.getLogger(PUEUserDetailsService.class);
	
	/**
	 * 
	 * @param username
	 */
	public void registerUser(String username) {
		// check if db has username
		if (!isUserRegistered(username)) {
			// get user roles
			Set<PUERole> roles = new HashSet<PUERole>();
			roles.add(roleManager.getDao().findByRole(Constants.USER_ROLES.user.getAsString()));
			
			// create user
			PUEUser user = new PUEUser(username, true);
			user.setRoles(roles);
			userManager.getDao().save(user);
			
			logger.info("registered new user, " + username);
		} else {
			logger.info("username already registered.");
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isUserRegistered(String username) {

		// check if db has username
		if (userManager.getDao().findByUsername(username) == null) {
			return false;
		}
		
		return true;
	}

}
