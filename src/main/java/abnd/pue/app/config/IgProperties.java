package abnd.pue.app.config;

/**
 * 
 * TODO move properties to db?
 */
public class IgProperties {
	/**
	 * Config Properties
	 */
	private String clientId;
	private String clientSecret;
	private String redirectUri;
	
	/**
	 * @param clientId
	 * @param clientSecret
	 * @param redirectUri
	 */
	public IgProperties(String clientId, String clientSecret, String redirectUri)  {
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.redirectUri = redirectUri;
	}
	
	/**
	 * @return
	 */
	public String getClientId() { return clientId; }
	
	/**
	 * @return
	 */
	public String getClientSecret() { return clientSecret; }
	
	/**
	 * @return
	 */
	public String getRedirectUri() { return redirectUri; }

}
