package abnd.pue.auth.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;

public class PUEUsernameAuthToken implements Authentication, CredentialsContainer {
	private static final long serialVersionUID = 3490203862062130744L;
	private boolean authenticated;
	private PUEUserDetails userDetails;
	private List<GrantedAuthority> authorities;

	/**
	 * @param userDetails
	 * @param authorities
	 */
	public PUEUsernameAuthToken(PUEUserDetails userDetails, List<GrantedAuthority> authorities) {
		this.userDetails = userDetails;
		this.authenticated = false;
		this.authorities = new ArrayList<GrantedAuthority>(authorities);
	}
	
	/* (non-Javadoc)
	 * @see java.security.Principal#getName()
	 */
	@Override
	public String getName() {
		// return user name
		return userDetails.getUsername();
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.CredentialsContainer#eraseCredentials()
	 */
	@Override
	public void eraseCredentials() {
		// nothing to do
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.Authentication#getAuthorities()
	 */
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.Authentication#getCredentials()
	 */
	@Override
	public String getCredentials() {
		// no credentials
		return "";
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.Authentication#getDetails()
	 */
	@Override
	public PUEUserDetails getDetails() {
		return userDetails;
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.Authentication#getPrincipal()
	 */
	@Override
	public PUEUserDetails getPrincipal() {
		// return user details
		return userDetails;
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.Authentication#isAuthenticated()
	 */
	@Override
	public boolean isAuthenticated() {
		return authenticated;
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.Authentication#setAuthenticated(boolean)
	 */
	@Override
	public void setAuthenticated(boolean authenticated) throws IllegalArgumentException {
		this.authenticated = authenticated;
	}

}
