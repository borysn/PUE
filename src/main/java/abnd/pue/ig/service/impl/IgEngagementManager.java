/**
 * 
 */
package abnd.pue.ig.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import abnd.pue.app.service.intf.DaoManager;
import abnd.pue.ig.dao.impl.IgEngagementDao;
import abnd.pue.ig.dao.intf.IgDao;

public class IgEngagementManager implements DaoManager<IgDao> {
	@Autowired
	private IgEngagementDao igEngagementDao;
	
	@Override
	public IgEngagementDao getDao() {
		return igEngagementDao;	
	}

}
