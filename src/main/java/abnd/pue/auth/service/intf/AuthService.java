package abnd.pue.auth.service.intf;

public interface AuthService {
	/**
	 * @param username
	 */
	public void grantAuthToken(String username);
}
