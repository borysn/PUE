package abnd.pue.ig.service.impl;

import org.jinstagram.auth.InstagramAuthService;
import org.jinstagram.auth.oauth.InstagramService;
import org.springframework.stereotype.Service;

/**
 * 
 * A singleton wrapper class for the InstagramService object 
 * from the jinstagram api. 
 *
 */
@Service
public class IgService {
	// instagram service
	private InstagramService instance;

	/**
	 * <p>
	 * Set InstagramService session objeigProperties.getRct, and return it. If the
	 * InstagramService object has already been added to the session
	 * return it.
	 * </p>
	 * @param session
	 */
	public IgService(String clientId, String clientSecret, String redirectUri) {
		// build ig auth service from ig abnd.pue.app.config properties
		InstagramAuthService igAuthService = new InstagramAuthService();
		igAuthService.apiKey(clientId);
		igAuthService.apiSecret(clientSecret);
		igAuthService.callback(redirectUri);
		instance = igAuthService.build();
	}
	
	/**
	 * 
	 * @return InstagramService
	 */
	public InstagramService getInstance() {
		return instance;
	}
}
