package abnd.pue.ig.dao.impl;

import javax.servlet.http.HttpSession;

import org.jinstagram.Instagram;
import org.jinstagram.auth.model.Token;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import abnd.pue.app.config.Constants;
import abnd.pue.ig.dao.intf.IgObjectDao;

public class IgObjectDaoImpl implements IgObjectDao {

	/**
	 * Get access token from session and build IgService
	 * 
	 * @return Instagram object
	 */
	public Instagram get() {
		// get current user session
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
	    HttpSession session =  attr.getRequest().getSession(true); 
	    
	    // build ig object
		return new Instagram((Token)session.getAttribute(Constants.INSTAGRAM_TOKEN));
	}
}
