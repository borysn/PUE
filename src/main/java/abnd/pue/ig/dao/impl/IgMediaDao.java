package abnd.pue.ig.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.jinstagram.entity.comments.MediaCommentsFeed;
import org.jinstagram.entity.common.Pagination;
import org.jinstagram.entity.likes.LikesFeed;
import org.jinstagram.entity.users.feed.MediaFeed;
import org.jinstagram.entity.users.feed.MediaFeedData;
import org.jinstagram.exceptions.InstagramException;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import abnd.pue.ig.dao.intf.IgDao;
import abnd.pue.ig.dao.intf.IgObjectDao;
import abnd.pue.ig.model.IgMedia;
import abnd.pue.ig.service.intf.IgMediaToJson;

public class IgMediaDao implements IgDao {
	@Autowired
	private IgMediaToJson igMediaToJson;
	@Autowired
	private IgObjectDao igObject;
	@Autowired
	private IgProfileDao igProfileDao;
	
	/**
	 * 
	 * @return
	 * @throws InstagramException
	 */
	public JsonArray getMostRecentMedia() throws InstagramException {
		// get recent media list
		MediaFeed feed = igObject.get().getRecentMediaFeed(igProfileDao.getUserId());
		List<MediaFeedData> mediaList =  feed.getData();
		// init return
		JsonArray mostRecentMedia = igMediaToJson.convert(mediaList);
		
		return mostRecentMedia;
	}
	
	/**
	 * TODO what happens if jinstagram API cannot paginate?
	 * 
	 * @return
	 * @throws InstagramException
	 */
	public JsonArray getInitialMediaSet() throws InstagramException {
		// init media feed pages
		List<MediaFeedData> mediaList = new ArrayList<MediaFeedData>();
		MediaFeed feedPg1 = new MediaFeed();
		MediaFeed feedPg2 = new MediaFeed();
		MediaFeed feedPg3 = new MediaFeed();
		
		// get media feeds and add media pages to media list
		feedPg1 = igObject.get().getRecentMediaFeed(igProfileDao.getUserId());
		
		if (!feedPg1.getData().isEmpty()) {
			mediaList.addAll(feedPg1.getData());
		}
		
		if (feedPg1.getPagination().getNextUrl() != null) {
			feedPg2 = igObject.get().getRecentMediaNextPage(feedPg1.getPagination());
			mediaList.addAll(feedPg2.getData());
		}
		
		if (feedPg2.getPagination().getNextUrl() != null) {
			feedPg3 = igObject.get().getRecentMediaNextPage(feedPg2.getPagination());
			mediaList.addAll(feedPg3.getData());
		}
		
		// init return
		JsonArray mostRecentMedia = igMediaToJson.convert(mediaList);
		return mostRecentMedia;
	}
	
	/**
	 * 
	 * @param lastImgId
	 * 
	 * @return
	 * @throws InstagramException
	 */
	public JsonArray getNextPageOfMedia(String lastImgId) throws InstagramException {
		// init media feed pages
		List<MediaFeedData> mediaList = new ArrayList<MediaFeedData>();
		MediaFeed mediaFeed = new MediaFeed();
		
		Pagination pagination = new Pagination();	
		pagination.setNextMaxId(lastImgId);
		
		mediaFeed = igObject.get().getRecentMediaFeed(igProfileDao.getUserId(), 18, null, lastImgId, null, null);
		
		// init return
		JsonArray nextMediaPage = new JsonArray();
		
		// determine if there is another page
		// if there isn't then return pagination complete
		// otherwise return next page of media
		if (mediaFeed.getData().isEmpty()) {
			nextMediaPage = returnPaginationComplete();
		} else {
			mediaList.addAll(mediaFeed.getData());
			nextMediaPage = igMediaToJson.convert(mediaList);
		}
		
		return nextMediaPage;
	}
	
	/**
	 * 
	 * @param imgId
	 * @return
	 * @throws InstagramException
	 */
	public JsonArray getSingleMedia(String imgId) throws InstagramException {
		// init return
		JsonArray media = new JsonArray();
		
		// get media likes
		LikesFeed likesFeed = igObject.get().getUserLikes(imgId);
		MediaCommentsFeed commentsFeed = igObject.get().getMediaComments(imgId);
		List<String> hashtags = igObject.get().getMediaInfo(imgId).getData().getTags();
		
		// convert media list to json
		media = igMediaToJson.convert(likesFeed, commentsFeed, hashtags);
		
		return media;
	}
	
	/**
	 * 
	 * @return
	 */
	private JsonArray returnPaginationComplete() {
		// init return
		JsonArray paginationComplete = new JsonArray();
		
		// create json indicating pagination is complete
		JsonObject json = new JsonObject();
		json.addProperty(IgMedia.PROPERTIES.paginationComplete.getAsString(), "true");
		
		// add json to return array
		paginationComplete.add(json);
		
		return paginationComplete;
	}
	
}
