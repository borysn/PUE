package abnd.pue.auth.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationDetailsSource;

import abnd.pue.auth.service.impl.PUEUserDetailsService;

public class PUEAuthDetailsSource implements AuthenticationDetailsSource<String , PUEUserDetails> {
	@Autowired
	PUEUserDetailsService pueUserDetailsService;
	
	/* (non-Javadoc)
	 * @see org.springframework.security.authentication.AuthenticationDetailsSource#buildDetails(java.lang.Object)
	 */
	@Override
	public PUEUserDetails buildDetails(String username) {
		return pueUserDetailsService.loadUserByUsername(username);
	}

}
