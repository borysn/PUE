package abnd.pue.auth.dao.impl;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import abnd.pue.app.dao.AbstractDao;
import abnd.pue.auth.dao.intf.PUERoleDao;
import abnd.pue.auth.model.PUERole;

public class PUERoleDaoImpl extends AbstractDao<PUERole> implements PUERoleDao {
	
	/**
	 * @param sessionFactory
	 */
	public PUERoleDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	
	/* (non-Javadoc)
	 * @see abnd.pue.dashboard.dao.intf.PUERoleDao#save(abnd.pue.dashboard.model.PUERole)
	 */
	@Override
	@Transactional
	public void save(PUERole role) {
		persist(role);
	}
	
	/* (non-Javadoc)
	 * @see abnd.pue.dashboard.dao.intf.PUERoleDao#save(java.util.Set)
	 */
	@Override
	@Transactional
	public void save(Set<PUERole> roles) {
		for (PUERole role : roles) {
			persist(role);
		}
	}

	/* (non-Javadoc)
	 * @see abnd.pue.dashboard.dao.intf.PUERoleDao#deleteById(int)
	 */
	@Override
	@Transactional
	public void deleteById(int id) {
		Query query = getSession().createSQLQuery("delete from roles where role_id = :id");
		query.setInteger("id", id);
		query.executeUpdate();
	}
	
	/* (non-Javadoc)
	 * @see abnd.pue.dashboard.dao.intf.PUERoleDao#update(abnd.pue.dashboard.model.PUERole)
	 */
	@Override
	@Transactional
	public void update(PUERole role) {
		getSession().update(role);
	}

	/* (non-Javadoc)
	 * @see abnd.pue.dashboard.dao.intf.PUERoleDao#findById(int)
	 */
	@Override
	@Transactional
	public PUERole findById(int id) {
		
		Criteria criteria = getSession().createCriteria(PUERole.class);
		criteria.add(Restrictions.eq("role_id", id));
		
		return (PUERole)criteria.uniqueResult();
	}

	/* (non-Javadoc)
	 * @see abnd.pue.dashboard.dao.intf.PUERoleDao#findByUsername(java.lang.String)
	 */
	@Override
	@Transactional
	public Set<PUERole> findByUserId(long userId) {
		
		Query query = getSession()
				.createSQLQuery("select role from roles where role_id = "
						        + "(select user_roles.role_id from user_roles "
						        + "where user_roles.user_id = :id)");
		query.setBigInteger("id", BigInteger.valueOf(userId));
		
		return getSet(query);
	}

	/* (non-Javadoc)
	 * @see abnd.pue.dashboard.dao.intf.PUERoleDao#findByRole(java.lang.String)
	 */
	@Override
	@Transactional
	public PUERole findByRole(String role) {
		
		Criteria criteria = getSession().createCriteria(PUERole.class);
		criteria.add(Restrictions.eq("role", role)); 
		
		return (PUERole)criteria.uniqueResult();
	}
	
	/**
	 * 
	 * @param query
	 * @return
	 */
	private Set<PUERole> getSet(Query query) {

		Set<PUERole> roles = new HashSet<>();
		
		for (final Object o : query.list()) {
			roles.add(new PUERole((String)o));
		}
		
		return roles;
	}

}
