package abnd.pue.logging.aop.ig;

import org.aspectj.lang.JoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IgOauthAspect {
	private static final Logger logger = LoggerFactory.getLogger(IgOauthAspect.class);
	
	/**
	 * 
	 * @param joinPoint
	 */
	public void logBefore(JoinPoint joinPoint) {
		logger.info("IgOauthAspect.logBefore() : " + joinPoint.getSignature().getName());
	}
	
	/**
	 * 
	 * @param joinPoint
	 */
	public void logAfter(JoinPoint joinPoint) {
		logger.info("IgOauthAspect.logAfter() : " + joinPoint.getSignature().getName());
	}

}
