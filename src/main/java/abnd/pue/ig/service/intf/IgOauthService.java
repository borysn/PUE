package abnd.pue.ig.service.intf;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public interface IgOauthService {
	/**
	 * 
	 * @param request
	 * @return String username
	 */
	public String processCallbackRequest(HttpServletRequest request, HttpSession session, String code);
}
