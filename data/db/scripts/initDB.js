/*
 * 
 * This is the mongo db initializer script. 
 * Edit vars according to your mongo installation. 
 * Run from console...
 * 
 * $ mongo initDB.js
 * 
 * Warning: this will drop the existing database
 * 
 */
var host = 'localhost';
var port = '27017';
var username = 'root';
var password = 'password';

/**
 * checkDBExists
 */
var checkDBExists = function(db) {
	if (db.users.count() > 0) {
		return true;
	}
	
	return false;
}

/**
 * Drop database
 */
var dropDB = function(db, dbname) {
	db = db.getSiblingDB(dbname);
	db.dropDatabase();
}

/**
 * create users collection
 */
var createUsersCollection = function(db) {
	db.users.save(
			// test user
			user = ({
				username: '$testusername',
				enabled: true
			})
	);
}

/**
 * create user_roles collection
 */
var createUserRolesCollection = function(db) {
	db.user_roles.save(
			// user_roll
			user_roll = ({ 
				username: '$testusername',
				role: 'ROLE_USER'
			})
	);
}

/**
 * initialize database
 */
var initDB = function(db) {
	// create 
	createUserRolesCollection(db);
	createUsersCollection(db);
}

/**
 * create database
 */
var createDB = function() {
	// do not change
	var dbname = 'pue';
	var connectLine = host+':'+port+'/admin';

	// connect to database
	var db = connect(connectLine);
	db.auth(username, password);
	
	// check if db already exists
	if (checkDBExists(db)) {
		dropDB(db, dbname);
	}
	
	// create db
	db = db.getSiblingDB(dbname);
	
	// init db
	initDB(db);
	
	// logout
	db.logout();
}

// create database
createDB();
