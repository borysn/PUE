package abnd.pue.ig.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import abnd.pue.ig.service.intf.IgOauthService;

@Controller
@RequestMapping(value = "/ig/oauth")
public class IgOauthController {  
	@Autowired
	private IgOauthService igOauthService;
	
	private static final Logger logger = LoggerFactory.getLogger(IgOauthController.class);

	/**
	 * 
	 * @param request
	 * @return String
	 * @throws IOException
	 */
	@RequestMapping(value = "/callback", method = RequestMethod.GET)
	public void igCallback(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException {
		// get code parameter
		String code = request.getParameter("code");
		// attempt to get user name from callback
        String username = igOauthService.processCallbackRequest(request, session, code);

        // if username isn't empty redirect to user page
        if (!username.isEmpty()) {
        	logger.info("username set: " + username);
        	response.sendRedirect("/" + username + "/dashboard");
        	return;
        }
        
        logger.info("no username set");
        
        // otherwise return to index TODO error display
        response.sendRedirect("/login");
        return;
	}
	
}
