package abnd.pue.app.config;

import java.io.Serializable;

/**
 * 
 * TODO move properties to db?
 */
public class Constants implements Serializable { 
	private static final long serialVersionUID = -275837040312616142L;
	/**
	 * Http Session Attributes
	 */
	public static final String INSTAGRAM_TOKEN = "igToken";
	public static final String INSTAGRAM_USERNAME = "igUsername";
	public static final String SESSION_ID = "sessionId";
	
	/**
	 * app name
	 */
	public static final String APP_NAME = "PUE for Instagram";
	
	/**
	 * View names
	 */
	public static enum VIEW_NAMES {
		// init enum vals
		login("Login"),
		privacyPolicy("PrivacyPolicy"),
		dashboard("Dashboard"),
		profile("Profile"),
		assets("Assets");
		
		private final String name;
		
		/**
		 * 
		 * @param String name
		 */
		private VIEW_NAMES(String name) { this.name = name; }
		/**
		 * 
		 * @return String name
		 */
		public String getAsString() { return name; }
	}
	
	/**
	 * user roll names
	 */
	public static enum USER_ROLES {
		// init enum vals
		user("ROLE_USER"),
		anonymous("ROLE_ANONYMOUS");
		
		private final String name;
		
		/**
		 * 
		 * @param String name
		 */
		private USER_ROLES(String name) { this.name = name; }
		/**
		 * 
		 * @return String name
		 */
		public String getAsString() { return name; }
	}
	
}
