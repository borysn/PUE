$(document).ready(function() {
	// flag to track whether ajax request was already fired due to scroll
	var ajaxInProgress = false;
	var paginationComplete = false;
	
	var showLoadingWheel = function() {
		// show only if loading-assets class doesn't exist
		if (!$('.loading-assets-container').length) {
			// create html
			var $container = $('<div>', {class : 'loading-assets-container'});
			var $content = $('<div>', {class : 'uil-spin-css loading-wheel', style : '-webkit-transform:scale(0.64);padding-top:0px;margin: 0 auto;margin-top: -20px;'});
			$content.append('<div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div>');
			$container.append($content);
			//$container.append('<span class="loading-text">Loading...</span>');
			// append html
			$('.media-feed').append($container)
			// scroll to end of .loading-assets in-order to fully display loading gif
			$(window).scrollTo($('.loading-assets-container'));
		}
	}
	
	var removeLoadingWheel = function() {
		$('.loading-assets-container').remove();
	}
	
	var getLastImgId = function() {
		return $('.infinite-scroll div:last-child').find('a').attr('data-imgid');
	}
	
	var getMoreMedia = function() {
		// make ajax call to rest controller if not already fired
		if (!ajaxInProgress && !paginationComplete) {
			// otherwise set ajax in progress
			ajaxInProgress = true;
			
			// make ajax call
			$.ajax({
				url: '/ig/' + username + '/media?lastImgId=' + getLastImgId(),
				type: 'GET',
				dataType: 'json',
				error: function() {
					// ajax request no longer running due to failure
					ajaxInProgress = false;
					removeLoadingWheel();
				},
				success: function(jsonData) {
					// process json data
					$.each(jsonData, function(index, images) {
						// process each element from data
						$.each(images, function(i, image) {
							// check if pagination is complete
							if (image.paginationComplete === "true") {
								removeLoadingWheel();
								paginationComplete = true;
								return;
							}
								
							// build new html
							html = $('<div class="btn asset-thumbnail-offset">')
							.append($('<a class="pueAsset" href="#" data-toggle="modal"' + 
									'data-target="#pueSessionModal" data-imgurl="' + image.lowResUrl + '" data-imgid="' + image.id +'" >')
									.append('<img class="img-rounded asset-thumbnail" src="' + image.thumbnailUrl + '"/>'));
							
							// append html
							$('.infinite-scroll').append(html);
						});
						// ajax request complete
						ajaxInProgress = false;
					});
				}
			});
		}
	}
	
	// detect scroll to bottom of page	
	$(window).on('scroll', function() {
		// get scroll height
		scrollHeight = $(document).height();
		// get scroll position
		scrollPosition = $(window).height() + $(window).scrollTop();
		
		if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
			if (!ajaxInProgress && !paginationComplete) {
				showLoadingWheel();
				getMoreMedia();
			}
		} else {
			removeLoadingWheel();
		}
	});
	
});