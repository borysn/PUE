package abnd.pue.auth.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import abnd.pue.auth.model.PUERole;
import abnd.pue.auth.model.PUEUser;
import abnd.pue.auth.model.PUEUserDetails;

@Service
public class PUEUserDetailsService implements UserDetailsService {  
	@Autowired
	private PUEUserManager userManager;
	@Autowired
	private PUERoleManager roleManager;
	
	Logger logger = LoggerFactory.getLogger(PUEUserDetailsService.class);
	
	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
	 */
	@Override
	public PUEUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// check security context for null
		if (SecurityContextHolder.getContext() != null) {
			// get user and users
			PUEUser user = userManager.getDao().findByUsername(username);
			
			// check for null
			if (user == null) {
				logger.error("could not load user by username: " + username);
				throw new UsernameNotFoundException(username);
			}
			
			// get user roles
			Set<PUERole> roles = roleManager.getDao().findByUserId(user.getId());
			
			// create userDetails
			return createUserDetails(user, roles);
		}
		
		return null;
	}
	
	/**
	 * 
	 * @param username
	 * @return
	 */
	public List<GrantedAuthority> getAuthorities(long userId) {
		// get user roles
		Set<PUERole> userRoles = roleManager.getDao().findByUserId(userId);
		List<GrantedAuthority> result = buildGrantedAuthorityList(userRoles);
		
		return result;
	}
	
	/**
	 * 
	 * @param user
	 * @param users
	 * @return
	 */
	private PUEUserDetails createUserDetails(PUEUser user, Set<PUERole> roles) {
		// get authorities list
		List<GrantedAuthority> authorities = buildGrantedAuthorityList(roles);
		PUEUserDetails userDetails = new PUEUserDetails(user.getUsername(), user.isEnabled(), authorities);
		
		return userDetails;
	}

	/**
	 * 
	 * @param roles
	 * @return
	 */
	private List<GrantedAuthority> buildGrantedAuthorityList(Set<PUERole> roles) {
		Set<GrantedAuthority> setAuths = new HashSet<>();
		
		// build user's authorities
		for (PUERole userRole : roles) {
			setAuths.add(new SimpleGrantedAuthority(userRole.getRole()));
		}
		
		List<GrantedAuthority> result = new ArrayList<>(setAuths);
		
		return result;
	}
}
