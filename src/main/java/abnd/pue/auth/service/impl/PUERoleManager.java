package abnd.pue.auth.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import abnd.pue.app.service.intf.DaoManager;
import abnd.pue.auth.dao.intf.PUERoleDao;

@Service
public class PUERoleManager implements DaoManager<PUERoleDao> {
	@Autowired
	private PUERoleDao roleDAO;

	/* (non-Javadoc)
	 * @see abnd.pue.dashboard.service.intf.DaoManager#getDAO()
	 */
	@Override
	public PUERoleDao getDao() {
		return roleDAO;
	}

}
