package abnd.pue.ig.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import abnd.pue.app.service.intf.DaoManager;
import abnd.pue.ig.dao.impl.IgProfileDao;

@Service
public class IgProfileManager implements DaoManager<IgProfileDao> {
	@Autowired
	private IgProfileDao igProfileDao;
	
	@Override
	public IgProfileDao getDao() {
		return igProfileDao;
	}

}
