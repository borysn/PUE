package abnd.pue.ig.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.jinstagram.entity.likes.LikesFeed;
import org.jinstagram.entity.media.MediaInfoFeed;
import org.jinstagram.entity.users.basicinfo.UserInfo;
import org.jinstagram.entity.users.feed.MediaFeed;
import org.jinstagram.entity.users.feed.MediaFeedData;
import org.jinstagram.exceptions.InstagramException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class IgEngagementService {
	@Autowired
	private IgObjectManager igObjectManager;
	
	private Logger logger = LoggerFactory.getLogger(IgEngagementService.class);
	
	/**
	 * 
	 * @param username
	 * @return
	 */
	public Map<String, String> getSingleUnlikedMedia(String username, String id) {
		// init return
		Map<String, String> media = new HashMap<String, String>();
		
		try {
			// get initial media feed 4 images
			MediaFeed mediaFeed = igObjectManager.getDao().get().getRecentMediaFeed(id, 4, null, null, null, null);
			
			while (media.isEmpty()) {
				// mediaId tracker
				String mediaId = new String();
				
				// step through users feed, find one media not liked by the current user
				for (int i = 0; i < mediaFeed.getData().size(); i++) {
					MediaFeedData mediaFeedData = mediaFeed.getData().get(i);
					mediaId = mediaFeedData.getId();
					MediaInfoFeed mediaInfoFeed = igObjectManager.getDao().get().getMediaInfo(mediaId);
					// if found image user hasn't liked before
					// add it to the return map and return
					if (!mediaInfoFeed.getData().isUserHasLiked()) {
						// get profile picture
						UserInfo userInfo = igObjectManager.getDao().get().getUserInfo(id);
						// get media url
						String mediaUrl = mediaFeedData.getImages().getLowResolution().getImageUrl();
						// add return properties to map
						media.put("mediaId", mediaId);
						media.put("mediaUrl", mediaUrl);
						media.put("imgUserProfilePic", userInfo.getData().getProfilePicture());
						
						/**
						 * application is running in sandboxed mode
						 * likes and comments are currently disabled for 
						 * this application.
						 */
						// like back the media through instagram api and return
						/*try {
							LikesFeed feed = likeSingleMedia(media);
							if (!feed.getUserList().isEmpty()) {
								return media;
							}
						} catch (InstagramException e) {
							logger.debug(e.getMessage());
						}*/

						return media;
						// something went wrong with liking back media, so return null;
						//return null;
					}
				}
				
				// no match, get next set of data
				mediaFeed = igObjectManager.getDao().get().getRecentMediaFeed(id, 4, null, mediaId, null, null);
				
				// check for end of media 
				if (mediaFeed.getData().isEmpty()) {
					return null;
				}
				
			}
			
		} catch (InstagramException e) {
			// error getting media
			// TODO null
			logger.debug(e.getMessage());
			return null;
		}
		
		return media;
	}
	
	/**
	 * 
	 * @param userdata
	 * @return
	 */
	public LikesFeed likeSingleMedia(Map<String, String> userdata) throws InstagramException {
		// like back media
		LikesFeed feed = igObjectManager.getDao().get().setUserLike(userdata.get("mediaId"));
		
		return feed;
	}

}
