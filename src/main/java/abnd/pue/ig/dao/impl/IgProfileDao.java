package abnd.pue.ig.dao.impl;

import org.jinstagram.entity.users.basicinfo.UserInfoData;
import org.jinstagram.exceptions.InstagramException;
import org.springframework.beans.factory.annotation.Autowired;

import abnd.pue.ig.dao.intf.IgDao;
import abnd.pue.ig.dao.intf.IgObjectDao;

public class IgProfileDao implements IgDao {
	@Autowired
	private IgObjectDao igObject;
	
	/**
	 * 
	 * @return
	 * @throws InstagramException
	 */
	public UserInfoData getUserData() throws InstagramException {
		return igObject.get().getCurrentUserInfo().getData();
	}
	
	/**
	 * 
	 * @return
	 * @throws InstagramException
	 */
	public String getUserId() throws InstagramException {
		return getUserData().getId();
	}
	
	/**
	 * 
	 * @return
	 * @throws InstagramException
	 */
	public String getUserName() throws InstagramException {
		return getUserData().getUsername();
	}
	
	/**
	 * 
	 * @return
	 * @throws InstagramException
	 */
	public String getFullName() throws InstagramException {
		return getUserData().getFullName();
	}
	
	/**
	 * 
	 * @return
	 * @throws InstagramException
	 */
	public String getBio() throws InstagramException {
		return getUserData().getBio();
	}
	
	/**
	 * 
	 * @return
	 * @throws InstagramException
	 */
	public String getWebsite() throws InstagramException {
		return getUserData().getWebsite();
	}
	
	/**
	 * 
	 * @return
	 * @throws InstagramException
	 */
	public String getProfilePicture() throws InstagramException {
		return getUserData().getProfilePicture();
	}
}
