package abnd.pue.auth.service.impl;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import abnd.pue.app.config.Constants;
import abnd.pue.auth.model.PUEUsernameAuthToken;

@Service
public class PUEUsernameAuthFilter implements Filter {

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		// do nothing
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		// check security context for null
		if (SecurityContextHolder.getContext() == null) {
			chain.doFilter(req, res);
			return;
		}
		
		// get http servlet request and response objects
		HttpServletRequest request = (HttpServletRequest)req;
        HttpServletResponse response = (HttpServletResponse) res;
        
		// check active session
		String activeIdSessionValue = (String)request.getSession().getAttribute(Constants.SESSION_ID);
    	
		// check session secure
		if (StringUtils.isNotBlank(activeIdSessionValue)) {
	        // get auth object
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			// get servlet path string
			String servletPath = request.getServletPath();
			
			// if user is logged in, send them back to the user page
			// unless: they are logging out
			if (servletPath.matches("(?:/login)") && (auth instanceof PUEUsernameAuthToken)) {
				// get username for dynamic url
				String username = (String)request.getSession().getAttribute(Constants.INSTAGRAM_USERNAME);
				response.sendRedirect("/" + username + "/dashboard");
				return;
			} 
		}
		
		chain.doFilter(request, response);
		return;
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig config) throws ServletException {
		// do nothing
	}

}
