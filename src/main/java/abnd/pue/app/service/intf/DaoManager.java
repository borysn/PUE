package abnd.pue.app.service.intf;

public interface DaoManager<T> {
	
	/**
	 * @return
	 */
	public T getDao();
	
}
