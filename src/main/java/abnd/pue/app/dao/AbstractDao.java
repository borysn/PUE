package abnd.pue.app.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class AbstractDao<T> {
	@Autowired
	private SessionFactory sessionFactory;
	
	public AbstractDao(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	/**
	 * 
	 * @return
	 */
	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	/**
	 * 
	 * @param entity
	 */
	public void persist(Object entity) {
		getSession().persist(entity);
	}
	
	/**
	 * 
	 * @param entity
	 */
	public void delete(Object entity) {
		getSession().delete(entity);
	}
	
}
