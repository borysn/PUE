package abnd.pue.ig.model;

public class IgMedia {
	
	// MediaDataFeed relevant properties
	// TODO externalize properties? or automate somehow? 
	public static enum PROPERTIES {
		// property names
		id("id"), 
		caption("caption"),
		lowResUrl("lowResUrl"),
		stdResUrl("stdResUrl"),
		thumbnailUrl("thumbnailUrl"),
		paginationComplete("paginationComplete"),
		comments("comments"),
		likes("likes"),
		tags("tags");
		
		private String property;
		
		private PROPERTIES(String property) { this.property = property; }
		public String getAsString() { return this.property; }
	}
	
}
