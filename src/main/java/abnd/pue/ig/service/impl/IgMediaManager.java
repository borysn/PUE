package abnd.pue.ig.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import abnd.pue.app.service.intf.DaoManager;
import abnd.pue.ig.dao.impl.IgMediaDao;

@Service
public class IgMediaManager implements DaoManager<IgMediaDao> {
	@Autowired
	private IgMediaDao igMediaDao;
	
	@Override
	public IgMediaDao getDao() {
		return igMediaDao;
	}

}
