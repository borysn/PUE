package abnd.pue.auth.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import abnd.pue.app.service.intf.DaoManager;
import abnd.pue.auth.dao.intf.PUEUserDao;

@Service
public class PUEUserManager implements DaoManager<PUEUserDao> {
	@Autowired
	private PUEUserDao userDAO;
	
	/* (non-Javadoc)
	 * @see abnd.pue.dashboard.service.intf.DaoManager#getDao()
	 */
	@Override
	public PUEUserDao getDao() {
		return userDAO;
	}

}
