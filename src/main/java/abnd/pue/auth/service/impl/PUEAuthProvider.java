package abnd.pue.auth.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import abnd.pue.auth.model.PUEUserDetails;
import abnd.pue.auth.model.PUEUsernameAuthToken;

@Service
public class PUEAuthProvider implements AuthenticationProvider {

	/* (non-Javadoc)
	 * @see org.springframework.security.authentication.AuthenticationProvider#authenticate(org.springframework.security.core.Authentication)
	 */
	@Override
	public Authentication authenticate(Authentication auth) throws AuthenticationException {
		List<GrantedAuthority> authorities = new ArrayList<>(auth.getAuthorities());
		return new PUEUsernameAuthToken((PUEUserDetails)auth.getPrincipal(), authorities);
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.authentication.AuthenticationProvider#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(Class<?> auth) {
		return auth.equals(PUEUsernameAuthToken.class);
	}
}
