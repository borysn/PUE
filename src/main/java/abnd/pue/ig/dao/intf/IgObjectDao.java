package abnd.pue.ig.dao.intf;

import org.jinstagram.Instagram;

public interface IgObjectDao {
	
	/**
	 * Get access token from session and build IgService
	 * 
	 * @return Instagram object
	 */
	public Instagram get();
}
