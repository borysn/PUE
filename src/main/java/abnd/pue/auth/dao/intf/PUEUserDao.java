package abnd.pue.auth.dao.intf;

import java.util.Set;

import abnd.pue.auth.model.PUEUser;

public interface PUEUserDao {

	/**
	 * @param user
	 */
	void save(PUEUser user);
	
	/**
	 * @param id
	 */
	void deleteById(int id);
	
	/**
	 * @param user
	 */
	void update(PUEUser user);
	
	/**
	 * @return
	 */
	Set<PUEUser> getAll();
	
	/**
	 * @param id
	 * @return
	 */
	PUEUser findByUserId(int id);
	
	/**
	 * @param username
	 * @return
	 */
	PUEUser findByUsername(String username);
	
}
