$('document').ready(function() {
	
	var otherUserLogin = function() {
		// open up a hidden iframe to execute instagram logout
		// currently instagram oauth does not support logout
		iframe = document.createElement("IFRAME");
		iframe.setAttribute("src", "http://instagram.com/accounts/logout");
		iframe.style.visibility = "hidden";
		document.body.appendChild(iframe);
		$('.other-user-login').css({visibility:'hidden'});
		window.location.href="/logout";
	}
	
	$('.logout').click(function(){ otherUserLogin(); });
	
});