package abnd.pue.auth.model;

import java.util.HashSet;
import java.util.Set;

public class PUEUser implements java.io.Serializable {
	private static final long serialVersionUID = -7816885431090052503L;
	private long id;
	private String username;
	private boolean enabled;
	private Set<PUERole> roles = new HashSet<>();
	
	/**
	 * 
	 */
	public PUEUser() {
		this.id = 0L;
		this.username = "";
		this.enabled = false;
	}
	
	/**
	 * 
	 * @param username
	 * @param enabled
	 * @param users
	 */
	public PUEUser(String username, boolean enabled) {
		this.username = username;
		this.enabled = enabled;
	}
	
	/**
	 * 
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * @param enabled 
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	/**
	 * 
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
	/**
	 * 
	 * @param users
	 */
	public void setRoles(Set<PUERole> roles) {
		this.roles = roles;
	}
	
	/**
	 * 
	 * @return
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getUsername() {
		return this.username;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isEnabled() {
		return this.enabled;
	}
	
	/**
	 * 
	 * @return
	 */
	public Set<PUERole> getRoles() {
		return roles;
	}
	
}
