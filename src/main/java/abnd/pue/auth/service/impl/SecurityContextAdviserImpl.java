package abnd.pue.auth.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import abnd.pue.auth.service.intf.SecurityContextAdviser;

@Service
public class SecurityContextAdviserImpl implements SecurityContextAdviser {
	  @Autowired
	  private AuthenticationTrustResolver authTrustResolver;

	/* (non-Javadoc)
	 * @see abnd.pue.auth.service.intf.SecurityContextAdviser#isCurrentAuthAnonymous()
	 */
	@Override
	public boolean isCurrentAuthAnonymous() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return authTrustResolver.isAnonymous(auth);
	}

}
