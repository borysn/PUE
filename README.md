# PUE
Personal User Engagement for instagram
[pue-borysn.rhcloud.com](http://pue-borysn.rhcloud.com)

# Description
A spring web mvc project aimed at making personal
instagram user engagement simpler, and more effective, 
without sacrificing the instagram user experience.

# Wiki
For development info, check the wiki.

[Wiki Home](https://gitlab.com/borysn/PUE/wikis/home)
    
[Wiki Index](https://gitlab.com/borysn/PUE/wikis/wiki-index)

