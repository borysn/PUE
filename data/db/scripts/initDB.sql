/*
 * 
 * This is the mysql db initializer script. 
 * Warning this script will drop the entire database.
 * Run using...
 * 
 * $ mysql -u username -p < initDB.sql
 * 
 * Warning: this will drop the existing database
 * 
 */
-- check if db exists, if it does drop it
drop database if exists pue;

-- create db
create database pue;

-- select db
use pue;

-- create users table
create table users (
	user_id bigint(11) not null auto_increment,
  	username varchar(45) not null,
  	enabled boolean not null,
  	primary key (user_id)
) engine=innodb auto_increment=1 default charset=utf8;

create table roles (
  	role_id bigint(11) not null auto_increment,
  	role varchar(45) not null, 
  	primary key (role_id)
) engine=innodb auto_increment=1 default charset=utf8;

-- create user_roles table
create table user_roles (
	user_id bigint(11) not null,
	role_id bigint(11) not null,
	primary key (user_id, role_id), 
    constraint fk_user_id foreign key (user_id) references users(user_id),
    constraint fk_role_id foreign key (role_id) references roles(role_id)
) engine=innodb default charset=utf8;

-- index users on username
create index usersOnUsernames on users(username);

-- index roles on role
create index rolesOnRole on roles(role);

-- insert initial roles
insert into roles (role) values ('ROLE_USER');
insert into roles (role) values ('ROLE_ANONYMOUS');