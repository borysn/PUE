package abnd.pue.auth.service.intf;

public interface SecurityContextAdviser {

	/**
	 * 
	 * @return
	 */
	public boolean isCurrentAuthAnonymous();
	
}
