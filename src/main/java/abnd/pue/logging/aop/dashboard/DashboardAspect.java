package abnd.pue.logging.aop.dashboard;

import org.aspectj.lang.JoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DashboardAspect {
	private static final Logger logger = LoggerFactory.getLogger(DashboardAspect.class);
	
	/**
	 * 
	 * @param joinPoint
	 */
	public void logBefore(JoinPoint joinPoint) {
		logger.info("DashboardController.logBefore() : " + joinPoint.getSignature().getName());
	}
	
	/**
	 * 
	 * @param joinPoint
	 */
	public void logAfter(JoinPoint joinPoint) {
		logger.info("DashboardController.logAfter() : " + joinPoint.getSignature().getName());
	}

}
