// sidebar accordion 
$(function() {
	$('#nav-accordion').dcAccordion({
		eventType : 'click',
		autoClose : true,
		//saveState : true,
		disableLink : true,
		speed : 'slow',
		showCount : false,
		autoExpand : true,
		//cookie: 'nav-accordion-pue',
		classExpand : 'dcjq-current-parent'
	});
});

// general scripts
var Script = function() {
	// begin screen hidden
	var wSize = $(window).width();
	if (wSize <= 768) {
		$('#container').addClass('sidebar-close');
		$('#sidebar > ul').hide();
	}
	
	// sidebar dropdown menu auto scrolling
	$('#sidebar .sub-menu > a').click(function() {
		var o = ($(this).offset());
		diff = 250 - o.top;
		if (diff > 0)
			$('#sidebar').scrollTo('-=' + Math.abs(diff), 500);
		else
			$('#sidebar').scrollTo('+=' + Math.abs(diff), 500);
	});

	// sidebar toggle
	$(function() {
		function responsiveView() {
			var wSize = $(window).width();
			if (wSize <= 768) {
				$('#container').addClass('sidebar-close');
				$('#sidebar > ul').hide();
			}

			if (wSize > 768) {
				$('#container').removeClass('sidebar-close');
				$('#sidebar > ul').show();
			}
		}
		$(window).on('load', responsiveView);
		$(window).on('resize', responsiveView);
	});

	$('.sidebar-toggle-box').click(function() {
		if ($('#sidebar > ul').is(':visible') === true) {
			$('#main-content').css({
				'margin-left' : '0px'
			});
			$('#sidebar').css({
				'margin-left' : '-210px'
			});
			$('#sidebar > ul').hide();
			$('#main-content').addClass('sidebar-closed');
		} else {
			$('#main-content').css({
				'margin-left' : '210px'
			});
			$('#sidebar > ul').show();
			$('#sidebar').css({
				'margin-left' : '0'
			});
			$('#main-content').removeClass('sidebar-closed');
		}
	});

	// custom scrollbar
	$('#sidebar').niceScroll({
		styler : 'fb',
		cursorcolor : '#ffffff',
		cursorwidth : '3',
		cursorborderradius : '10px',
		background : '#161616',
		spacebarenabled : false,
		cursorborder : '',
		railpadding: {top: 60, right: 0, left: 0, bottom: -60}
	});

	$('html').niceScroll({
		styler : 'fb',
		cursorcolor : '#ffffff',
		cursorwidth : '6',
		cursorborderradius : '10px',
		background : '#161616',
		spacebarenabled : false,
		cursorborder : '',
		zindex : '1000',
		railpadding: {top: 60, right: 0, left: 0, bottom: -60}
	});

	// widget tools
	jQuery('.panel .tools .fa-chevron-down')
			.click(function() {
						var el = jQuery(this).parents('.panel').children('.panel-body');
						if (jQuery(this).hasClass('fa-chevron-down')) {
							jQuery(this).removeClass('fa-chevron-down').addClass('fa-chevron-up');
							el.slideUp(200);
						} else {
							jQuery(this).removeClass('fa-chevron-up').addClass('fa-chevron-down');
							el.slideDown(200);
						}
					});

	jQuery('.panel .tools .fa-times').click(function() {
		jQuery(this).parents('.panel').parent().remove();
	});

	// fixing a bug with dcAccordion
	$('#nav-accordion').ready(function() {
		/* 
		 * (while logged in) when leaving the dashboard, hitting the index page, and getting re-routed back
		 * to the dashboard, dcAccordion does not reset active class menu item.
		 */
		pathName = window.location.pathname;
		reggex = new RegExp('(?:(?:\\w+)(?:/dashboard)$)');
		// if we are at dashboard, we must reset active class
		if (reggex.test(pathName)) {
			$('#nav-accordion').find('a').removeClass('active');
			$('#nav-accordion').find('a:eq(1)').addClass('active');
		}
	});

	// tool tips
	$('.tooltips').tooltip();

	// popovers
	$('.popovers').popover();
}();