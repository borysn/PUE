package abnd.pue.auth.dao.impl;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import abnd.pue.app.dao.AbstractDao;
import abnd.pue.auth.dao.intf.PUEUserDao;
import abnd.pue.auth.model.PUEUser;

public class PUEUserDaoImpl extends AbstractDao<PUEUser> implements PUEUserDao {
	
	/**
	 * @param sessionFactory
	 */
	public PUEUserDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	
	/* (non-Javadoc)
	 * @see abnd.pue.dashboard.dao.intf.PUEUserDao#save(abnd.pue.dashboard.model.PUEUser)
	 */
	@Override
	@Transactional
	public void save(PUEUser user) {
		persist(user);
	}

	/* (non-Javadoc)
	 * @see abnd.pue.dashboard.dao.intf.PUEUserDao#deleteById(int)
	 */
	@Override
	@Transactional
	public void deleteById(int id) {
		Query query = getSession().createSQLQuery("delete from users where user_id = :user_id");
		query.setInteger("user_id", id);
		query.executeUpdate();
	}

	/* (non-Javadoc)
	 * @see abnd.pue.dashboard.dao.intf.PUEUserDao#update(abnd.pue.dashboard.model.PUEUser)
	 */
	@Override
	@Transactional
	public void update(PUEUser user) {
		getSession().update(user);
	}

	/* (non-Javadoc)
	 * @see abnd.pue.dashboard.dao.intf.PUEUserDao#getAll()
	 */
	@Override
	@Transactional
	public Set<PUEUser> getAll() {
		Criteria criteria = getSession().createCriteria(PUEUser.class);
		return getSet(criteria);
	}

	/* (non-Javadoc)
	 * @see abnd.pue.dashboard.dao.intf.PUEUserDao#findByUserId(int)
	 */
	@Override
	@Transactional
	public PUEUser findByUserId(int id) {
		Criteria criteria = getSession().createCriteria(PUEUser.class);
		criteria.add(Restrictions.eq("user_id", id));
		return (PUEUser)criteria.uniqueResult();
	}

	/* (non-Javadoc)
	 * @see abnd.pue.dashboard.dao.intf.PUEUserDao#findByUsername(java.lang.String)
	 */
	@Override
	@Transactional
	public PUEUser findByUsername(String username) {
		Criteria criteria = getSession().createCriteria(PUEUser.class);
		criteria.add(Restrictions.eq("username", username));
		return (PUEUser)criteria.uniqueResult();
	}

	/**
	 * 
	 * @param query
	 * @return
	 */
	private Set<PUEUser> getSet(Criteria criteria) {

		Set<PUEUser> users = new HashSet<>();
		
		for (final Object o : criteria.list()) {
			users.add((PUEUser)o);
		}
		
		return users;
	}
	
}
