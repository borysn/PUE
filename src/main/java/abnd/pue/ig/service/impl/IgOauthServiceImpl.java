package abnd.pue.ig.service.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.jinstagram.Instagram;
import org.jinstagram.auth.model.Token;
import org.jinstagram.auth.model.Verifier;
import org.jinstagram.auth.oauth.InstagramService;
import org.jinstagram.exceptions.InstagramException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import abnd.pue.app.service.intf.AppService;
import abnd.pue.auth.service.intf.AuthService;
import abnd.pue.ig.service.intf.IgOauthService;
import abnd.pue.register.service.intf.RegisterService;

@Service
public class IgOauthServiceImpl implements IgOauthService {
	@Autowired
	private RegisterService registerService;
	@Autowired
	private AuthService authService;
	@Autowired
	private IgService igService;
	@Autowired
	private AppService appService;
	
	private Logger logger = LoggerFactory.getLogger(IgOauthServiceImpl.class);
	
	/**
	 * 
	 * @param request
	 * @return String username
	 */
	public String processCallbackRequest(HttpServletRequest request, HttpSession session, String code) {
		try {
			// get igService
			InstagramService igServiceObject = igService.getInstance();
					
			// get response code
	        logger.info("callback code: " + code);
	        // get verifier
	        Verifier verifier = new Verifier(code);
	        logger.info("callback verifier: " + verifier.getValue());
	        
	        // get access token
	        Token accessToken = igServiceObject.getAccessToken(null, verifier);
	        logger.info("callback accessToken: " + accessToken);
	        // create ig object
	        // TODO NULL object
	        Instagram instagram = null;
	        
	        // catch access token error
	        try {
	        	instagram = new Instagram(accessToken);
	        } catch (Exception e) {
	        	// log error, return empty String
	        	logger.error(e.getMessage());
	        	return "";
	        }
	        
	        // get username
	        String username = instagram.getCurrentUserInfo().getData().getUsername();
	        
	        // if necessary, register user
	        if(!registerService.isUserRegistered(username)) {
	        	registerService.registerUser(username);
	        	logger.info("user is registered: " + username);
	        }
	        
	        // grant auth token
	        authService.grantAuthToken(username);
	        
	        // set session attrs
	        appService.setSessionVars(username, accessToken);
	        
	        // return username
	        return username;
		} catch (InstagramException e) {
			logger.error(e.getMessage());
		}
		
		return "";
	}	

}
