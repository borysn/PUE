package abnd.pue.app.service.intf;

import org.jinstagram.auth.model.Token;

public interface AppService {
	
	public void setSessionVars(String username, Token accessToken);

}
