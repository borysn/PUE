package abnd.pue.auth.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jinstagram.auth.oauth.InstagramService;
import org.jinstagram.exceptions.InstagramException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import abnd.pue.app.config.Constants;
import abnd.pue.dashboard.controller.DashboardController;
import abnd.pue.ig.service.impl.IgProfileManager;
import abnd.pue.ig.service.impl.IgService;

@Controller
public class AuthController {
	@Autowired
	private IgProfileManager igProfileManager;
	@Autowired
	private IgService igService;
	
	private static final Logger logger = LoggerFactory.getLogger(DashboardController.class);
	
	/**
	 * TODO: move index to separate auth controller
	 * 
	 * @param request
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(HttpServletRequest request, HttpSession session) {
		// init return
		ModelAndView mav = new ModelAndView();
		// set view name
		mav.setViewName(Constants.VIEW_NAMES.login.getAsString().toLowerCase());
		logger.info("view name: " + Constants.VIEW_NAMES.login.getAsString());
		
		// get InstagramService and set session object
		InstagramService igServiceObject = igService.getInstance();
		
		// get auth url
		String authUrl = igServiceObject.getAuthorizationUrl(null);
		logger.info("auth url: " + authUrl);
		
		// head
		mav.addObject("pageTitle", Constants.APP_NAME);
		
		// content 
		mav.addObject("authUrl", authUrl);
		
		// check for auth error
		// TODO parameter type checking and execution abstraction.
		if (request.getParameterMap().containsKey("error") &&
		    request.getParameter("error").equals("auth")) {
			mav.addObject("errorMsg", "Authentication error.");
		}
		
		return mav;
	}
	
	
	/**
	 * Log user out
	 * 
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/logout", method=RequestMethod.GET)
	public String logout(HttpServletRequest request, HttpSession session) throws InstagramException {
		logger.debug("Logging out user: " + igProfileManager.getDao().getUserName());

		// invalidate session
		session.invalidate();
		
        // return to index
		return "redirect:/";
	}

	/**
	 * 
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/error", method=RequestMethod.GET)
	public void error(HttpServletResponse response) throws IOException {
		response.sendRedirect("redirect:/");
		return;
	}
	
	/**
	 * 
	 * @param response
	 */
	@RequestMapping(value = "/privacy_policy", method=RequestMethod.GET) 
	public ModelAndView privacyPolicy() {
		// init return
		ModelAndView mav = new ModelAndView();
		mav.setViewName(Constants.VIEW_NAMES.privacyPolicy.getAsString().toLowerCase());
		
		// head
		mav.addObject("pageTitle", Constants.APP_NAME);
		
		return mav;
	}
}
