package abnd.pue.ig.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import abnd.pue.app.service.intf.DaoManager;
import abnd.pue.ig.dao.intf.IgObjectDao;

@Service
public class IgObjectManager implements DaoManager<IgObjectDao> {
	@Autowired
	private IgObjectDao igObjectDao;
	
	@Override
	public IgObjectDao getDao() {
		return igObjectDao;
	}
}
