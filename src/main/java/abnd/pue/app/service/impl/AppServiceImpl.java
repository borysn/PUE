package abnd.pue.app.service.impl;

import javax.servlet.http.HttpSession;

import org.jinstagram.auth.model.Token;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import abnd.pue.app.config.Constants;
import abnd.pue.app.service.intf.AppService;

@Service
public class AppServiceImpl implements AppService {
	
	/* (non-Javadoc)
	 * @see abnd.pue.app.service.intf.AppService#setSessionVars(java.lang.String, org.jinstagram.auth.model.Token, java.lang.String)
	 */
	@Override
	public void setSessionVars(String username, Token accessToken) {
		// get session
		HttpSession session = getSession();
		
        // add token to session
        session.setAttribute(Constants.INSTAGRAM_TOKEN, accessToken);
        // add username to session
        session.setAttribute(Constants.INSTAGRAM_USERNAME, username);
        // add session id to session
        session.setAttribute(Constants.SESSION_ID, session.getId());
	}

	/**
	 * getSession();
	 * 
	 * Get current HttpSession
	 * 
	 * @return current HttpSession
	 */
	private HttpSession getSession() {
		// access RequestContextHolder
	    ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
	    return attr.getRequest().getSession(true); 
	}
}
