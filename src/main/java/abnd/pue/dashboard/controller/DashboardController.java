package abnd.pue.dashboard.controller;

import org.jinstagram.exceptions.InstagramException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonArray;

import abnd.pue.app.config.Constants;
import abnd.pue.ig.model.IgMedia;
import abnd.pue.ig.service.impl.IgMediaManager;
import abnd.pue.ig.service.impl.IgObjectManager;
import abnd.pue.ig.service.impl.IgProfileManager;

@Controller
@RequestMapping(value = "/{username}")
public class DashboardController {
	@Autowired
	private IgMediaManager igMediaManager;
	@Autowired
	private IgProfileManager igProfileManager;
	@Autowired
	private IgObjectManager igObject;
	
	private static final Logger logger = LoggerFactory.getLogger(DashboardController.class);
	
	/**
	 * TODO handling instagram exceptions? which layer?
	 * 
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/dashboard", method=RequestMethod.GET)
	public ModelAndView userDashboard() throws InstagramException {
		// init return view
		ModelAndView mav = new ModelAndView();
		// set view name
		mav.setViewName(Constants.VIEW_NAMES.dashboard.getAsString().toLowerCase());
		logger.info("view name: " + Constants.VIEW_NAMES.dashboard.getAsString());

		
		// retrieve view properties
		String fullName = igProfileManager.getDao().getFullName();
		String profilePicture = igProfileManager.getDao().getProfilePicture();
		String username = igProfileManager.getDao().getUserName();
		String viewName = Constants.VIEW_NAMES.dashboard.getAsString();
		int requestLimit = igObject.getDao().get().getCurrentUserInfo().getAPILimitStatus();
		int remainingLimit = igObject.getDao().get().getCurrentUserInfo().getRemainingLimitStatus();
		
		// add view properties to page
		mav.addObject("fullName", fullName);
		mav.addObject("profilePicture", profilePicture);
		mav.addObject("username", username);
		mav.addObject("pageTitle", username);
		mav.addObject("viewName", viewName);
		mav.addObject("requestLimit", requestLimit);
		mav.addObject("remainingLimit", remainingLimit);
		
		return mav;
	}
	
	/**
	 * 
	 * @return
	 * @throws InstagramException
	 */
	@RequestMapping(value = "/profile", method=RequestMethod.GET) 
	public ModelAndView userProfile() throws InstagramException {
		// init return view
		ModelAndView mav = new ModelAndView();
		// set view name
		mav.setViewName(Constants.VIEW_NAMES.profile.getAsString().toLowerCase());
		logger.info("view name: " + Constants.VIEW_NAMES.profile.getAsString());
		
		// retrieve view properties
		String viewName = Constants.VIEW_NAMES.profile.getAsString();
		String username = igProfileManager.getDao().getUserName();
		String bio = igProfileManager.getDao().getBio();
		String website = igProfileManager.getDao().getWebsite();
		mav.addObject("username", username);
		mav.addObject("bio", bio);
		mav.addObject("website", website);
		mav.addObject("viewName", viewName);
		
		// sidebar
		String fullName = igProfileManager.getDao().getFullName();
		String profilePicture = igProfileManager.getDao().getProfilePicture();
		mav.addObject("fullName", fullName);
		mav.addObject("profilePicture", profilePicture);

		// head
		mav.addObject("pageTitle", username);
		
		return mav;
	}
	
	/**
	 * 
	 * @return
	 * @throws InstagramException
	 */
	@RequestMapping(value = "/assets", method=RequestMethod.GET) 
	public ModelAndView userAssets() throws InstagramException {
		// init return view
		ModelAndView mav = new ModelAndView();
		// set view name
		mav.setViewName(Constants.VIEW_NAMES.assets.getAsString().toLowerCase());
		logger.info("view name: " + Constants.VIEW_NAMES.assets.getAsString());
		
		// retrieve view properties
		String username = igProfileManager.getDao().getUserName();
		String viewName = Constants.VIEW_NAMES.assets.getAsString();
		JsonArray mediaFeed = igMediaManager.getDao().getInitialMediaSet();
		
		mav.addObject("mediaFeed", mediaFeed);
		mav.addObject("viewName", viewName);
		mav.addObject("username", username);
		
		// json properties for media
		mav.addObject("lowResUrl", IgMedia.PROPERTIES.lowResUrl.getAsString());
		mav.addObject("stdResUrl", IgMedia.PROPERTIES.stdResUrl.getAsString());
		mav.addObject("caption", IgMedia.PROPERTIES.caption.getAsString());
		mav.addObject("id", IgMedia.PROPERTIES.id.getAsString());
		
		// sidebar
		String fullName = igProfileManager.getDao().getFullName();
		String profilePicture = igProfileManager.getDao().getProfilePicture();
		mav.addObject("fullName", fullName);
		mav.addObject("profilePicture", profilePicture);

		// head
		mav.addObject("pageTitle", username);
		
		return mav;
	}
}