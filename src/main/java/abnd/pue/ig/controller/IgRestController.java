package abnd.pue.ig.controller;

import javax.servlet.http.HttpServletRequest;

import org.jinstagram.exceptions.InstagramException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import abnd.pue.ig.service.impl.IgEngagementManager;
import abnd.pue.ig.service.impl.IgMediaManager;

/**
 * 
 * url prefix
 * /ig/*
 *
 */
@RestController 
@RequestMapping("/{username}")
public class IgRestController {
	@Autowired
	private IgMediaManager igMediaManager;
	@Autowired
	private IgEngagementManager igEngagementManager;
	
	/**
	 * 
	 * @param request
	 * @return
	 * @throws InstagramException
	 */
	@RequestMapping(value = "/media", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public JsonArray igMedia(HttpServletRequest request) throws InstagramException {
		// get lastImgId parameter
		String lastImgId = request.getParameter("lastImgId");
		
		// init return
		JsonArray media = igMediaManager.getDao().getNextPageOfMedia(lastImgId);
		
		return media;
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 * @throws InstagramException
	 */
	@RequestMapping(value = "/media/single", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public JsonArray igSingleMedia(HttpServletRequest request) throws InstagramException {
		// get lastImgId parameter
		String imgId = request.getParameter("imgId");
		
		// init return
		JsonArray media = igMediaManager.getDao().getSingleMedia(imgId);
		
		return media;
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 * @throws InstagramException
	 */
	@RequestMapping(value = "/engage/likeback", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public JsonArray likeBack(HttpServletRequest request) throws InstagramException {
		// get userData
		JsonArray userData = new JsonParser().parse(request.getParameter("userdata")).getAsJsonArray();
		
		// init return
		JsonArray media = igEngagementManager.getDao().igLikeBackAll(userData);
		
		return media;
	}
	
}
