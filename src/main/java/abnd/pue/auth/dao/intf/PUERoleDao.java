package abnd.pue.auth.dao.intf;

import java.util.Set;

import abnd.pue.auth.model.PUERole;

public interface PUERoleDao {

	/**
	 * @param role
	 */
	
	void save(PUERole role);
	
	/**
	 * @param roles
	 */
	void save(Set<PUERole> roles);
	
	/**
	 * @param id
	 */
	void deleteById(int id);
	
	/**
	 * @param role
	 */
	void update(PUERole role);

	/**
	 * @param id
	 * @return
	 */
	PUERole findById(int id);
	
	/**
	 * @param role
	 * @return
	 */
	PUERole findByRole(String role);
	
	/**
	 * @param username
	 * @return
	 */
	Set<PUERole> findByUserId(long userId); 
	
}
