package abnd.pue.auth.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import abnd.pue.auth.model.PUEUserDetails;
import abnd.pue.auth.model.PUEUsernameAuthToken;
import abnd.pue.auth.service.intf.AuthService;

@Service
public class AuthServiceImpl implements AuthService {
	@Autowired 
	private ApplicationContext appContext;

	/**
	 * Pragmatically set auth token
	 * 
	 * @param username
	 */
	public void grantAuthToken(String username) {
		SecurityContextHolder.clearContext();
		SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
		
		// pragmatic auth
		PUEUserDetailsService userDetailsService = (PUEUserDetailsService)appContext.getBean("pueUserDetailsService");
		PUEUserDetails userDetails = userDetailsService.loadUserByUsername(username);
		
		// no password
		Authentication auth = new PUEUsernameAuthToken(userDetails, userDetails.getAuthorities());
		auth.setAuthenticated(true);
		
		// set new security context
		securityContext.setAuthentication(auth);
		SecurityContextHolder.setContext(securityContext);
	}
}
