package abnd.pue.register.service.intf;

public interface RegisterService {

	/**
	 * @param username
	 */
	public void registerUser(String username);
	
	/**
	 * @param username
	 * @return
	 */
	public boolean isUserRegistered(String username);
	
}
