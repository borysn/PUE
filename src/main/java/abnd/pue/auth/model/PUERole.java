package abnd.pue.auth.model;

import java.util.HashSet;
import java.util.Set;

import abnd.pue.app.config.Constants;

public class PUERole implements java.io.Serializable {
	private static final long serialVersionUID = -8380820657195541207L;
	private long id;
	private String role;
	private Set<PUEUser> users = new HashSet<>();
	
	/**
	 * 
	 */
	public PUERole() {
		this.id = 0L;
		this.role = Constants.USER_ROLES.anonymous.getAsString();
	}
	
	/**
	 * @param role
	 */
	public PUERole(String role) {
		this.role = role;
	}
	
	/**
	 * 
	 * @param id
	 * @param role
	 */
	public PUERole(long id, String role) {
		this.id = id;
		this.role = role;
	}
	
	/**
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * @param role
	 */
	public void setRole(String role) {
		this.role = role;
	}
	
	/**
	 * @param users
	 */
	public void setUsers(Set<PUEUser> users) {
		this.users = users;
	}
	
	/**
	 * @return
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * @return
	 */
	public String getRole() {
		return role;
	}
	
	/**
	 * @return
	 */
	public Set<PUEUser> getUsers() {
		return this.users;
	}

}
