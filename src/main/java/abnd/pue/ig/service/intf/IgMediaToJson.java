package abnd.pue.ig.service.intf;

import java.util.List;

import org.jinstagram.entity.comments.MediaCommentsFeed;
import org.jinstagram.entity.likes.LikesFeed;
import org.jinstagram.entity.users.feed.MediaFeedData;
import org.jinstagram.exceptions.InstagramException;

import com.google.gson.JsonArray;

public interface IgMediaToJson {
	
	/**
	 * 
	 * @param mediaList
	 * @return
	 * @throws InstagramException
	 */
	public JsonArray convert(List<MediaFeedData> mediaList) throws InstagramException;
	public JsonArray convert(LikesFeed likes, MediaCommentsFeed comments, List<String> hashtags);
	
}
