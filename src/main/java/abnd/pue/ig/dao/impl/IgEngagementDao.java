package abnd.pue.ig.dao.impl;

import java.util.Iterator;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import abnd.pue.ig.dao.intf.IgDao;
import abnd.pue.ig.service.impl.IgEngagementService;

public class IgEngagementDao implements IgDao {
	@Autowired
	IgEngagementService igEngagementService;
	
	/**
	 * Return map of username:media_id for all images liked.
	 * 
	 * @param userData
	 * @return
	 */
	public JsonArray igLikeBackAll(JsonArray userData) {
		// init return
		JsonArray mediaData = new JsonArray();
		
		// step through userData using iterator
		Iterator<JsonElement> it = userData.iterator();
		while (it.hasNext()) {
			JsonObject data = it.next().getAsJsonObject();
			String username = data.get("username").getAsString();
			String id = data.get("id").getAsString();
			Map<String, String> media = igEngagementService.getSingleUnlikedMedia(username, id);
			if (media != null) {
				data.addProperty("mediaId", media.get("mediaId"));
				data.addProperty("mediaUrl", media.get("mediaUrl"));
				data.addProperty("imgUserProfilePic", media.get("imgUserProfilePic"));
				mediaData.add(data);
			}
		}
		
		return mediaData;
	}
	
}
