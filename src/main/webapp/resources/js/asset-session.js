$('document').ready(function() {
	// globals
	var likes = {};
	var comments = {};
	var hashtags = {};
	var sidePaneHtmlTarget = $('.modal-side-pane-content');
	var slider = {};
	var selectedComments = [];
	
	// hide puemneu
	var hidePueMenu = function() {
		$('#puemenu').css({
			'margin-left' : '-210px',
			'webkit-box-shadow' : '0 0 0 0px #00FF92'
		});
		$('.puemenu-background').hide();
		$('#puemenu > ul').hide();
	};
	
	// loading screen
	var showLoadingScreen = function() {
		// create loading wheel html
		var $container = $('<div>', {class : 'slide-show-loading-container'});
		var $content = $('<div>', {class : 'uil-spin-css', style : '-webkit-transform:scale(0.64);width:0px;height:0px;'});
		$content.append('<div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div>');
		$container.append($content);
		$('.modal-side-pane-background').css({visibility : 'visible'});
		$('.modal-side-pane-background').append($container);
	};
	
	var hideLoadingScreen = function() {
		$('.modal-side-pane-background').css({visibility : 'hidden'});
		$('.modal-side-pane-background').html("");
	};
	
	// slide show
	var createSlideShow = function() {
		// attach slide show and background
		$('#page').prepend('<aside id="slide-show-container"><div class="slide-show-background"></div><div class="slide-show"></div></aside>');
		
		// close slide show
		$('.slide-show').on('click', '#closeSlideShow', function() {
			closeSlideShow();
		});
		
		// close slide show
		$('.slide-show').on('click', '#closeSlideShowButton', function() {
			closeSlideShow();
		});
	};
	
	var closeSlideShow = function() {
		slider.destroySlider();
		slider = {};
		$('#slide-show-container').css({visibility: 'hidden'});
		$('#slide-show-container').html('');
	};
	
	// hide modal side pane
	var hideModalSidePane = function() {
		$('.modal-side-pane-background').css({visibility: 'hidden'});
		$('.modal-side-pane').css({visibility: 'hidden'});
		$(sidePaneHtmlTarget).html("");
	};
	
	// show modal side pane
	var showModalSidePane = function() {
		$('.modal-side-pane-background').css({visibility: 'visible'});
		$('.modal-side-pane').css({visibility: 'visible'});
		$('.modal-side-pane').focus();
	};
	
	// get asset data
	var getAssetData = function(modal, imgId) {
		// make ajax call
		$.ajax({
			url: '/ig/' + username + '/media/single?imgId=' + imgId,
			type: 'GET',
			dataType: 'json',
			error: function() {
				// ajax request no longer running due to failure
			},
			success: function(jsonData) {
				// set globals 
				likes = jsonData.elements[0].likes;
				comments = jsonData.elements[1].comments;
				hashtags = jsonData.elements[2].hashtags;

				// set likes count
				$('#total-likes').text('' + likes[0].totalLikes);
				// set comments count
				$('#total-comments').text('' + comments[0].totalComments);
				// set hashtags count
				$('#total-hashtags').text('' + hashtags[0].totalHashtags);
			}}
		);
	}; 
	
	// process json response
	var processJsonResponse = function(jsonData) {
		// create slide show dom
		createSlideShow();
		
		// html vars
		var $container = $('.slide-show');
		var $title = $('<div>', {class : 'slide-show-title'});
		var $desc = $('<div>', {class : 'slide-show-description'});
		var $content = $('<ul>', {class : 'bxslider'});
		
		// create title
		$title.append('<a href="javasript:void(0);">PUE</a> Session Slide Show');
		$title.append('<div><img id="img-user-profile-pic" src=""/><span id="img-username"></span></div>')
		// add title
		$container.append($title);
		
		// get slide show ready
		$.each(jsonData.elements, function(index, data){
			$content.append('<li><img data-username="' + data.username + '" data-profile-pic="' + 
					data.imgUserProfilePic + '" src="' + data.mediaUrl  + '"/></li>');
		});
		
		// add content to container
		$container.append($content);
		
		// add container to dom
		$('.slide-show').append($container);
		
		// finished processing hide loading wheel
		hideLoadingScreen();
		
		// display slide show
		$('.slide-show-background').css({visibility : 'visible'});
		$container.css({visibility : 'visible'});
		
		// get bx slider ready
		slider = $('.bxslider').bxSlider({
			randomStart: false,
			startSlide: 0,
			controls: true,
			useCss: false,
			auto: 'true',
			speed: '300',
			// call backs
			onSlideBefore: function() {
				// get slider variables
				total = slider.getSlideCount();
				current = slider.getCurrentSlide();
				
				// get the profile picture for the user of the img
				imgUsername = $('ul.bxslider li:nth-child(' + (current+2) + ') > img').data('username');
				profilePic = $('ul.bxslider li:nth-child(' + (current+2) + ') > img').data('profile-pic');
				$('#img-username').text(imgUsername);
				$('#img-user-profile-pic').attr('src', profilePic);
				
				// update slider count
				$('#currentPhoto').text('' + (current+1));
				
				if (total == (current+1)) {
					$('.bx-controls').removeClass('hide');
					slider.stopAuto();
					if (!$('#closeSlideShow').length > 0) {
						$('.slide-show').prepend('<button id="closeSlideShow" type="button" class="close">×</button>');
						$('.bx-controls').append('<button id="closeSlideShowButton">Close</button>');
					}
				}
			}
		});
		
		// create description
		$desc.append('<span>Viewing </span><span id="currentPhoto">1</span><span> out of ' + slider.getSlideCount() +'</span>');
		// add description
		$container.append($desc);

		// set username for first photo
		var imgUsername = $('ul.bxslider li:nth-child(2) > img').data('username');
		var profilePic = $('ul.bxslider li:nth-child(2) > img').data('profile-pic');
		$('#img-username').text(imgUsername);
		$('#img-user-profile-pic').attr('src', profilePic);
		
		// hide controls
		$('.bx-controls').addClass('hide');
		
	
	}
	
	// start off with hidden menu
	hidePueMenu();
	
	// pue-menu dropdown menu auto scrolling
	$('#puemenu .sub-menu > a').click(function() {
		var o = ($(this).offset());
		diff = 250 - o.top;
		if (diff > 0)
			$('#sidebar').scrollTo('-=' + Math.abs(diff), 500);
		else
			$('#sidebar').scrollTo('+=' + Math.abs(diff), 500);
	});

	// puemenu toggle 
	$('.puemenu-toggle-box').click(function() {
		if ($('#puemenu > ul').is(':visible') === true) {
			hidePueMenu();
		} else {
			$('.puemenu-background').show();
			$('#puemenu > ul').show();
			$('#puemenu').css({
				'margin-left' : '-15px',
				'webkit-box-shadow' : '0 0 0 1px #00FF92'
			});
		}
	});
	
	// puemenu background
	$('.puemenu-background').click(function() {
		hidePueMenu();
	});

	// custom scrollbar
	$('#puemenu').niceScroll({
		styler : 'fb',
		cursorcolor : '#ffffff',
		cursorwidth : '3',
		cursorborderradius : '10px',
		background : '#161616',
		spacebarenabled : false,
		cursorborder : '',
		railpadding: {top: 60, right: 0, left: 0, bottom: -60}
	});
	
	// side pane clear on focus out
	$('.modal-side-pane').focusout(function() {
		hideModalSidePane();
	});
	
	// close button for side pane
	$('.modal-side-pane button.close').click(function() {
		hideModalSidePane();
	});
	
	// likes count click
	// show users that liked the asset
	$('#likes').click(function(){
		// step through likes adding them to dom
		var $div = $('<div>', {class: 'data'});
		
		$.each(likes[1].userData, function(i, user){
			$div.append('<div class="modal-side-pane-item">' +
					'<span class="modal-side-pane-item-username">' + user.username +
					'</span></div>');
		});
		
		$(sidePaneHtmlTarget).append($div);
		showModalSidePane();
	});
	
	// comments count click
	// show comments on the asset
	$('#comments').click(function() {
		// step through comments adding them to dom
		var $div = $('<div>', {class: 'data'});
		
		$.each(comments[1].commentData, function(i, comment){
			$div.append('<div class="modal-side-pane-item">' +
					'<img class="modal-side-pane-item-pic" src="' + comment.pic + '"/>' +
					'<span class="modal-side-pane-item-username">' + comment.username + '</span>' +
					'<span class="modal-side-pane-item-text">' + comment.text + 
					'</span></div><hr/>');
		});
		
		$(sidePaneHtmlTarget).append($div);
		showModalSidePane();
	});
	
	// hashtag count click
	// show hastags on the asset
	$('#hashtags').click(function() {
		// step through hashtags adding them to dom
		var $div = $('<div>', {class: 'data'});
		
		$.each(hashtags[1].hashtagData, function(i, hashtag){
			$div.append('<div class="modal-side-pane-item">' +
					'<span class="modal-side-pane-item-hashtag">' + hashtag.tag +
					'</span></div>');
		});
		
		$(sidePaneHtmlTarget).append($div);
		showModalSidePane();
	});
	
	// select comment
	$('.comment').click(function(event) {
		// get comment id
		var id = $(this).attr('data-id');
		
		// chcek if selected
		if ($(this).hasClass('comment-selected')) {
			// get stored item index
			index = selectedComments.indexOf(id);
			// remove stored item
			if (index > -1) {
				selectedComments.splice(index, 1);
			}
			
			// unselect
			$(this).removeClass('comment-selected');
		} else {
			// select if less than 5 selected
			if (selectedComments.length < 5) {
				selectedComments.push(id);
				$(this).addClass('comment-selected');
			}
		}
	});
	
	var getComments = function() {
		// show comment selection modal
		$('#pueCommentSelectionModal').modal('show');
		
		// bring up modal-backdrop
		zindex = $('.modal-backdrop').css('z-index');
		$('.modal-backdrop').css('z-index', $('#pueCommentSelectionModal').css('z-index') - 1);
		
		// on dismis modal
		$('#pueCommentSelectionModal').on('hidden.bs.modal', function() {
			// reset .modal-backdrop zindex
			$('.modal-backdrop').css('z-index', zindex);
		});
		
		// save comments
		$('#saveComments').click(function(){
			// check if 5 comments have been selected
			if (selectedComments.length === 5) {
				// dismiss comment selection modal
				$('#pueCommentSelectionModal').modal('hide');
				
				// show loading screen
				showLoadingScreen();
				
				// make ajax call
				$.ajax({
					url: '/ig/' + username + '/engage/likeback?userdata=' + JSON.stringify(likes[1].userData),
					type: 'GET',
					dataType: 'json',
					error: function() {
						// ajax request no longer running due to failure
						hideLoadingScreen();
					},
					// process json response
					success: function(jsonData) {
						processJsonResponse(jsonData);
					}}
				);
				
				hidePueMenu();
			}
		});
	}

	// comment back
	$('#commentBackAll').click(function() {
		// show comment selection modal
		getComments();
	});
	
	// like back
	$('#likeBackAll').click(function() {
		// show loading screen
		showLoadingScreen();
		
		// make ajax call
		$.ajax({
			url: '/ig/' + username + '/engage/likeback?userdata=' + JSON.stringify(likes[1].userData),
			type: 'GET',
			dataType: 'json',
			error: function() {
				// ajax request no longer running due to failure
				hideLoadingScreen();
			},
			// process json response
			success: function(jsonData) {
				processJsonResponse(jsonData);
			}}
		);
		
		hidePueMenu();
	});
	
	// show pue session modal
	$('#pueSessionModal').on('show.bs.modal', function(event){
		var button = $(event.relatedTarget);
		var imgUrl = button.data('imgurl');
		var imgId = button.data('imgid');
		
		// get modal 
		var modal = $(this);
		// add img url to display image
		modal.find('#asset').attr('src', imgUrl);
		
		// display rest of the image data
		getAssetData(modal, imgId);
	});
	
});