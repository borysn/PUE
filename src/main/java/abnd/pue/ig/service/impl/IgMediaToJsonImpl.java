package abnd.pue.ig.service.impl;

import java.util.List;

import org.jinstagram.entity.comments.CommentData;
import org.jinstagram.entity.comments.MediaCommentsFeed;
import org.jinstagram.entity.common.User;
import org.jinstagram.entity.likes.LikesFeed;
import org.jinstagram.entity.users.feed.MediaFeedData;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import abnd.pue.ig.model.IgMedia;
import abnd.pue.ig.service.intf.IgMediaToJson;

@Service
public class IgMediaToJsonImpl implements IgMediaToJson {

	/**
	 * 
	 * @param mediaList
	 * @return
	 */
	@Override
	public JsonArray convert(List<MediaFeedData> mediaList) {
		// init return
		JsonArray jsonArray = new JsonArray();
		
		// TODO abstract to json building system
		for (MediaFeedData mediaFeedData : mediaList) {
			JsonObject jsonObject = new JsonObject();
			
			jsonObject.addProperty(IgMedia.PROPERTIES.id.getAsString(), mediaFeedData.getId());
			jsonObject.addProperty(IgMedia.PROPERTIES.caption.getAsString(), mediaFeedData.getCaption().getText());
			jsonObject.addProperty(IgMedia.PROPERTIES.lowResUrl.getAsString(), mediaFeedData.getImages().getLowResolution().getImageUrl());
			jsonObject.addProperty(IgMedia.PROPERTIES.stdResUrl.getAsString(), mediaFeedData.getImages().getStandardResolution().getImageUrl());
			jsonObject.addProperty(IgMedia.PROPERTIES.thumbnailUrl.getAsString(), mediaFeedData.getImages().getThumbnail().getImageUrl());
			
			jsonArray.add(jsonObject);
		}
		
		return jsonArray;
	}
	
	/**
	 * 
	 * @param likes
	 * @param comments
	 * @param hastags
	 * @return
	 */
	@Override
	public JsonArray convert(LikesFeed likes, MediaCommentsFeed comments, List<String> hashtags) {
		// init return
		JsonArray jsonArray = new JsonArray();
		
		// convert likes to json
		JsonObject likesData = new JsonObject();
		likesData.add("likes", convertLikesToJson(likes));
		// convert comments json
		JsonObject commentsData = new JsonObject();
		commentsData.add("comments", convertCommentsToJson(comments));
		// convert hashtags json
		JsonObject hashtagsData = new JsonObject();
		hashtagsData.add("hashtags", convertHashtagsToJson(hashtags));
		
		// add likes, comments, and hastags json to return 
		jsonArray.add(likesData);
		jsonArray.add(commentsData);
		jsonArray.add(hashtagsData);
		
		return jsonArray;
	}
	
	/**
	 * 
	 * @param likes
	 * @return
	 */
	private JsonArray convertLikesToJson(LikesFeed likes) {
		// init return
		JsonArray jsonArray = new JsonArray();
		
		// add total likes
		JsonObject totalLikes = new JsonObject();
		totalLikes.addProperty("totalLikes", likes.getUserList().size());
		jsonArray.add(totalLikes);
		
		// process user list
		JsonObject userLikes = new JsonObject();
		JsonArray userData = new JsonArray();
		// step through each users info that gave a like
		for (User user : likes.getUserList()) {
			JsonObject userInfo = new JsonObject();
			
			userInfo.addProperty("id", user.getId());
			userInfo.addProperty("username", user.getUserName());
			
			userData.add(userInfo);
		}
		userLikes.add("userData", userData);
		
		// add all user info
		jsonArray.add(userLikes);
		
		return jsonArray;
	}
	
	/**
	 * 
	 * @param comments
	 * @return
	 */
	private JsonArray convertCommentsToJson(MediaCommentsFeed comments) {
		// init return
		JsonArray jsonArray = new JsonArray();
		
		// get total comments
		JsonObject totalComments = new JsonObject();
		totalComments.addProperty("totalComments", comments.getCommentDataList().size());
		jsonArray.add(totalComments);
		
		// get comment data
		JsonArray commentArray = new JsonArray();
		JsonObject userComments = new JsonObject();
		// iterate though comments
		for (CommentData comment : comments.getCommentDataList()) {
			JsonObject commentData = new JsonObject();

			commentData.addProperty("userid", comment.getCommentFrom().getId());
			commentData.addProperty("username", comment.getCommentFrom().getUsername());
			commentData.addProperty("pic", comment.getCommentFrom().getProfilePicture());
			commentData.addProperty("text", comment.getText());
			
			commentArray.add(commentData);
		}
		
		// add comments data to return array
		userComments.add("commentData", commentArray);
		jsonArray.add(userComments);
		
		return jsonArray;
	}
	
	/**
	 * 
	 * @param hashtags
	 * @return
	 */
	private JsonArray convertHashtagsToJson(List<String> hashtags) {
		// init return
		JsonArray jsonArray = new JsonArray();
		
		// convert hashtags to json
		JsonObject hashtagTotal = new JsonObject();
		hashtagTotal.addProperty("totalHashtags", hashtags.size());
		
		JsonObject hashtagData = new JsonObject();
		JsonArray hashtagsArray = new JsonArray();
		
		for (String hashtag : hashtags) {
			JsonObject obj = new JsonObject();
			obj.addProperty("tag", hashtag);
			hashtagsArray.add(obj);
		}
		
		hashtagData.add("hashtagData", hashtagsArray);
		
		// add new json to return
		jsonArray.add(hashtagTotal);
		jsonArray.add(hashtagData);
		
		return jsonArray;
	}

}
